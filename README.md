# NodeAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.2.1 and 
then express was added to enable serving from express.

## Development server

Run `npm run watchdog` to run build with autorecompile on changes.
Run `npm run server` to serve app on localhost:3000.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Further help

The project uses Angular CLI check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md) for more information. 
All ng commands in terminal for any other angularcli project are applicable including tests.
