import { NodeAngularPage } from './app.po';

describe('node-angular App', () => {
  let page: NodeAngularPage;

  beforeEach(() => {
    page = new NodeAngularPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
